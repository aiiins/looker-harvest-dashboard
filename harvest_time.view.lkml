view: harvest_time {
  sql_table_name: airflow.harvest_time ;;

  dimension: billable_flag {
    type: number
    sql: ${TABLE}.BillableFlag ;;
  }

  dimension: billable_rate {
    type: number
    sql: ${TABLE}.BillableRate ;;
  }

  dimension: client_name {
    type: string
    sql: ${TABLE}.ClientName ;;
  }

  dimension: hours {
    type: number
    sql: ${TABLE}.Hours ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}.Notes ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.ProjectName ;;
  }

  dimension_group: spent {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.SpentDate ;;
  }

  dimension: task_name {
    type: string
    sql: ${TABLE}.TaskName ;;
  }

  dimension: time_entry_id {
    type: string
    sql: ${TABLE}.TimeEntryId ;;
  }

  dimension: user_name {
    type: string
    sql: ${TABLE}.UserName ;;
  }

  measure: hours_per_client {
    type: sum
    drill_fields: [user_name, client_name, project_name, task_name]
  }
}
